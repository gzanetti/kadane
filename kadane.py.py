'''
Assumptions:
- the input array contains numerical values (positive and negative)
- the input array contains at least one element.

We use Kadane's algorithm.

Time and Space Complexity Analysis of max_subarray algorithm:
    The algorithm is solved in linear time complexity O(n)
     1. The for-loop "for current_end, num in enumerate(nums):" is resolved in O(n) time with n number of elements of the input array.
     2.  nums[best_start:best_end] require allocating a new array and visit O(n) positions in the input array, hence it will account for O(n) time complexty of the algorithm.
     The space complexity of the algorithm is linear O(n) since we allocate at most a constant number of arrays of dimension n.
'''
import numpy as np

class UnitTestMaxSubarray: 
    
    def max_subarray(self, nums):
        best_sum = - np.Inf
        best_start = 0 
        best_end = 0
        current_sum = 0
        for current_end, num in enumerate(nums): #O(n)
            if current_sum <= 0:
                current_start = current_end
                current_sum = num
            else:
                current_sum += num
    
            if current_sum > best_sum:
                best_sum = current_sum
                best_start = current_start
                best_end = current_end + 1 
        return (nums[best_start:best_end], best_sum) #O(n)
    
    def test(self):
        in_array = [[-2,5,11,9,-3,-5,-6], [2,-6,3,-2,4], [-3], [5], [-3,-2,-1,-1]]
        out_array = [([5,11,9],25), ([3,-2,4],5), ([-3],-3), ([5], 5), ([-1], -1)]
        result = []
        for nums in in_array:
            result.append( self.max_subarray(nums) )
            
        for nums, nums_out in zip(result, out_array):
            if (nums[0] != nums_out[0]) or (nums[1] != nums_out[1]):
                return False
        return True
        

t = UnitTestMaxSubarray()
print(t.test())
        
